import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:news/models/category.dart';
import 'package:news/services/news_service.dart';
import 'package:news/theme/theme.dart';
import 'package:news/widgets/news_list.dart';
import 'package:provider/provider.dart';

class HeadlinesTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          _CategoryList(),
          _CategoryNewsList(),
        ],
      ),
    );
  }
}

class _CategoryNewsList extends StatelessWidget {
  const _CategoryNewsList({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    NewsService newsService = Provider.of<NewsService>(context);
    return Expanded(
      child: newsService.isLoading
          ? Center(child: CircularProgressIndicator())
          : NewsList(news: newsService.selectedCategoryNews),
    );
  }
}

class _CategoryList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    NewsService newsService = Provider.of<NewsService>(context);

    return Container(
      height: 80.0,
      child: ListView.builder(
        physics: BouncingScrollPhysics(),
        scrollDirection: Axis.horizontal,
        itemCount: newsService.categories.length,
        itemBuilder: (BuildContext context, int index) {
          return _CategoryListItem(
            category: newsService.categories[index],
            index: index,
          );
        },
      ),
    );
  }
}

class _CategoryListItem extends StatelessWidget {
  _CategoryListItem({
    Key key,
    @required this.index,
    @required this.category,
  }) : super(key: key);

  final Category category;
  final int index;

  @override
  Widget build(BuildContext context) {
    final String capitalizedName =
        '${category.name[0].toUpperCase()}${category.name.toLowerCase().substring(1)}';
    NewsService newsService = Provider.of<NewsService>(context);

    return GestureDetector(
      onTap: () {
        newsService.selectedCategory = category.name;
        newsService.getTopHeadlinesByCategory(category.name);
      },
      child: Container(
        padding: EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              child: Icon(
                category.icon,
                size: 20.0,
                color: newsService.selectedCategory == category.name
                    ? customTheme.accentColor
                    : Colors.black54,
              ),
              height: 40.0,
              width: 40.0,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.white,
              ),
            ),
            SizedBox(height: 5.0),
            Text(capitalizedName),
          ],
        ),
      ),
    );
  }
}
