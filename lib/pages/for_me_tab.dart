import 'package:flutter/material.dart';
import 'package:news/services/news_service.dart';
import 'package:news/widgets/news_list.dart';
import 'package:provider/provider.dart';

class ForMeTab extends StatefulWidget {
  @override
  _ForMeTabState createState() => _ForMeTabState();
}

class _ForMeTabState extends State<ForMeTab>
    with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    final newsService = Provider.of<NewsService>(context);

    return SafeArea(
      child: Container(
        child: newsService.headlines.length == 0
            ? Center(child: CircularProgressIndicator())
            : NewsList(news: newsService.headlines),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
