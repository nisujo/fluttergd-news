import 'package:flutter/material.dart';
import 'package:news/pages/for_me_tab.dart';
import 'package:news/pages/headlines_tab.dart';
import 'package:provider/provider.dart';

class TabsPage extends StatelessWidget {
  static final String routeName = 'tabs';

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => _NavigationModel(),
      child: Scaffold(
        body: _Pages(),
        bottomNavigationBar: _Navigation(),
      ),
    );
  }
}

class _Navigation extends StatelessWidget {
  const _Navigation({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final navigationModel = Provider.of<_NavigationModel>(context);

    return BottomNavigationBar(
      currentIndex: navigationModel.currentPage,
      onTap: (int index) => navigationModel.currentPage = index,
      items: [
        BottomNavigationBarItem(
          icon: Icon(Icons.person_outline),
          label: 'Para ti',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.public),
          label: 'Encabezados',
        ),
      ],
    );
  }
}

class _Pages extends StatelessWidget {
  const _Pages({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final navigationModel = Provider.of<_NavigationModel>(context);

    return PageView(
      controller: navigationModel.pageController,
      onPageChanged: (int index) => navigationModel.currentPage = index,
      physics: BouncingScrollPhysics(),
      children: [
        ForMeTab(),
        HeadlinesTab(),
      ],
    );
  }
}

class _NavigationModel with ChangeNotifier {
  int _currentPage = 0;
  PageController _pageController = PageController(
    initialPage: 0,
  );

  int get currentPage => _currentPage;

  set currentPage(int value) {
    _currentPage = value;

    pageController.animateToPage(
      value,
      duration: Duration(milliseconds: 250),
      curve: Curves.easeOut,
    );

    notifyListeners();
  }

  PageController get pageController => _pageController;
}
