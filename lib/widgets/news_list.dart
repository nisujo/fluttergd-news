import 'package:flutter/material.dart';
import 'package:news/models/article.dart';
import 'package:news/theme/theme.dart';

class NewsList extends StatelessWidget {
  final List<Article> news;

  NewsList({Key key, @required this.news}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: news.length,
      itemBuilder: (BuildContext context, int index) {
        return _NewsCard(
          article: news[index],
          index: index,
        );
      },
    );
  }
}

class _NewsCard extends StatelessWidget {
  final Article article;
  final int index;

  const _NewsCard({
    Key key,
    @required this.article,
    @required this.index,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 10.0),
      child: Column(
        children: [
          _NewsCardSource(index: index, article: article),
          _NewsCardTitle(article: article),
          _NewsCardThumbnail(article: article),
          _NewsCardDescription(article: article),
          _NewsCardActions(),
          Divider(),
        ],
      ),
    );
  }
}

class _NewsCardActions extends StatelessWidget {
  const _NewsCardActions({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        RaisedButton(
          elevation: 0.0,
          shape: StadiumBorder(),
          color: customTheme.accentColor,
          child: Icon(Icons.star_border),
          onPressed: () {},
        ),
        SizedBox(width: 10.0),
        RaisedButton(
          elevation: 0.0,
          shape: StadiumBorder(),
          color: Colors.blue,
          child: Icon(Icons.more),
          onPressed: () {},
        ),
      ],
    );
  }
}

class _NewsCardDescription extends StatelessWidget {
  const _NewsCardDescription({
    Key key,
    @required this.article,
  }) : super(key: key);

  final Article article;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: Text(
        article.description ?? '',
        maxLines: 3,
        overflow: TextOverflow.ellipsis,
      ),
    );
  }
}

class _NewsCardThumbnail extends StatelessWidget {
  const _NewsCardThumbnail({
    Key key,
    @required this.article,
  }) : super(key: key);

  final Article article;

  @override
  Widget build(BuildContext context) {
    return FadeInImage(
      placeholder: AssetImage('assets/images/giphy.gif'),
      image: article.urlToImage == null
          ? AssetImage('assets/images/no-image.png')
          : NetworkImage(article.urlToImage),
      fit: BoxFit.cover,
      height: 200.0,
      width: double.infinity,
    );
  }
}

class _NewsCardTitle extends StatelessWidget {
  const _NewsCardTitle({
    Key key,
    @required this.article,
  }) : super(key: key);

  final Article article;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text(
        article.title,
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 20.0,
        ),
      ),
    );
  }
}

class _NewsCardSource extends StatelessWidget {
  const _NewsCardSource({
    Key key,
    @required this.index,
    @required this.article,
  }) : super(key: key);

  final int index;
  final Article article;

  @override
  Widget build(BuildContext context) {
    String count = (index + 1).toString();
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: Row(
        children: [
          Text(
            count,
            style: TextStyle(color: customTheme.accentColor),
          ),
          SizedBox(width: 5.0),
          Text(article.source.name),
        ],
      ),
    );
  }
}
