import 'package:flutter/material.dart';
import 'package:news/models/article.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_dotenv/flutter_dotenv.dart' as dotenv;
import 'package:news/models/category.dart';
import 'package:news/models/news.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class NewsService with ChangeNotifier {
  final String _url = 'https://newsapi.org/v2';
  final String _apiKey = dotenv.env['NEWS_API_KEY'];
  final String _country = 'co';

  String _selectedCategory = 'business';
  List<Article> headlines = [];
  Map<String, List<Article>> newsCategoryArticles = {};
  bool _isLoading = true;

  List<Category> categories = [
    Category(FontAwesomeIcons.building, 'business'),
    Category(FontAwesomeIcons.tv, 'entertainment'),
    Category(FontAwesomeIcons.idCard, 'general'),
    Category(FontAwesomeIcons.headSideVirus, 'health'),
    Category(FontAwesomeIcons.vials, 'science'),
    Category(FontAwesomeIcons.futbol, 'sports'),
    Category(FontAwesomeIcons.memory, 'technology'),
  ];

  NewsService() {
    categories.forEach((category) {
      newsCategoryArticles[category.name] = List();
    });

    getTopHeadlines();
    getTopHeadlinesByCategory(_selectedCategory);
  }

  get selectedCategory => _selectedCategory;

  set selectedCategory(String value) {
    _selectedCategory = value;
    _isLoading = true;
    notifyListeners();
  }

  get selectedCategoryNews => newsCategoryArticles[_selectedCategory];

  get isLoading => _isLoading;

  Future<void> getTopHeadlines() async {
    String url = '$_url/top-headlines?country=$_country&apiKey=$_apiKey';
    http.Response response = await http.get(url);
    News news = News.fromRawJson(response.body);

    headlines.addAll(news.articles);
    notifyListeners();
  }

  Future<void> getTopHeadlinesByCategory(String category) async {
    if (newsCategoryArticles[category].length > 0) {
      _isLoading = false;
      return;
    }

    String url =
        '$_url/top-headlines?country=$_country&category=$_selectedCategory&apiKey=$_apiKey';
    http.Response response = await http.get(url);
    News news = News.fromRawJson(response.body);

    newsCategoryArticles[category].addAll(news.articles);
    _isLoading = false;

    notifyListeners();
  }
}
