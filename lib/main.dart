import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart' as dotenv;
import 'package:news/services/news_service.dart';
import 'package:provider/provider.dart';
import 'pages/tabs_page.dart';
import 'theme/theme.dart';

Future<void> main() async {
  await dotenv.load();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => NewsService()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter News',
        theme: customTheme,
        initialRoute: TabsPage.routeName,
        routes: {
          TabsPage.routeName: (_) => TabsPage(),
        },
      ),
    );
  }
}
