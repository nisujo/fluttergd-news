# Flutter News

Aplicación que muestra noticias desde el api [News API](https://newsapi.org/).

Al descargar el repositorio, se debe copiar el archivo **.env.example** a **.env** y llenar los datos de configuración con los datos pertinentes.

![Screenshot](screenshot.png "Screenshot")